module.exports = {
  bracketSpacing: true, // 花括号内使用空格包裹
  singleQuote: true, // 使用单引号
  semi: false, // 不能在语句末尾添加分号
  arrowParens: 'always', // 为单行箭头函数的参数添加圆括号 如：(x) => x
  trailingComma: 'none', // 严格模式，数组项末尾不能有逗号
  endOfLine: 'auto', // 结尾换行\n \r \n\r 保证Windows下文件的LF格式
  htmlWhitespaceSensitivity: 'ignore',
  printWidth: 120
}
