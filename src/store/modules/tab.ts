import { MenuItem } from '~/types/ui/MenuItem'
import { defineStore } from 'pinia'
import config from '~/config'

export default defineStore({
  id: 'tab',
  state: () => {
    let data = {
      current: null as MenuItem,
      tabs: [] as MenuItem[]
    }
    return data
  },
  actions: {
    contains(name: String, caption: String | undefined, path: String | undefined) {
      if (this.tabs.length == 0) {
        this.tabs.push({
          name: 'index',
          caption: '首页',
          path: '/index'
        })
      }
      for (let i = 0; i < this.tabs.length; i++) {
        if (this.tabs[i].name == name) {
          return this.tabs[i]
        }
      }
      const newItem = {
        name,
        caption,
        path
      } as MenuItem
      if (this.tabs.length == config.maxOpenModuleTableCount) {
        this.tabs.splice(1, 1)
      }
      this.tabs.push(newItem)
      return newItem
    },
    clear() {
      this.current = null
      this.tabs = []
    }
  }
})
