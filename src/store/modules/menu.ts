import { MenuItem } from '~/types/ui/MenuItem'
import { defineStore } from 'pinia'

export default defineStore({
  id: 'menu',
  state: () => {
    let data = {
      current: null as MenuItem
    }
    return data
  },
  actions: {
    change(): void {}
  }
})
