import userStore from './modules/user'
import menuStore from './modules/menu'
import tabStore from './modules/tab'

export { userStore, menuStore, tabStore }
