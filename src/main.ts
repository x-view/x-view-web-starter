import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { setupAuthDirective } from './directive'
import { createPinia } from 'pinia'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import '~/styles/index.scss'
import 'uno.css'

//按需引入css
import 'element-plus/theme-chalk/src/message.scss'
import 'element-plus/theme-chalk/src/message-box.scss'
import 'element-plus/theme-chalk/src/notification.scss'
import 'element-plus/theme-chalk/src/menu.scss'
import 'element-plus/theme-chalk/src/pagination.scss'
import 'element-plus/theme-chalk/src/tree.scss'
import 'element-plus/theme-chalk/src/table.scss'
import 'element-plus/theme-chalk/src/button.scss'

const pinia = createPinia()
const app = createApp(App).use(router).use(pinia)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
setupAuthDirective(app)
app.mount('#app')
