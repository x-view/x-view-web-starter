export interface ITableColumn {
  label: string
  prop: string
  align?: 'left' | 'center' | 'right'
  width?: string | number
  slot?: boolean
  fixed?: boolean
  minWidth?: number
  formatter?: () => {}
}
