import { PageQueryResult } from '~/types/PageQueryResult'
import { ITableColumn } from './ITableColumn'

class TableProps<TDataRow> {
  tableColumns: ITableColumn[] = []
  tableData: any[] = []
  selection: TDataRow[] = []
  showPagination?: boolean = true
  pageSize?: number = 15
  pageCurrent?: number = 1
  recordsTotal?: number = 0
  tableLoading?: boolean = false
  isSelection?: boolean = true
  tooltips?: boolean = true
  isSingle?: boolean = false
  hasIndex?: boolean = false
  rowKey?: string = ''
  stripe?: boolean = true
  border?: boolean = false

  /**
   * 填充分页结构数据
   * @param pagedData 具备标准分页结构的数据
   */
  fillPagedData(pagedData: PageQueryResult<TDataRow>) {
    this.pageCurrent = pagedData.currentPage
    this.recordsTotal = pagedData.recordsTotal
    this.tableData = pagedData.data
  }
}
export { TableProps }
