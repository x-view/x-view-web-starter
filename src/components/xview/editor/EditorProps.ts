import { EditType } from '~/types/ui/EditType'

class EditorProps<TEditEntity> {
  editType: EditType = 'view'
  model: TEditEntity = null
  visable: boolean = false

  insert(editEntity: TEditEntity) {
    this.editType = 'insert'
    this.model = editEntity
    this.visable = true
  }
  update(editEntity: TEditEntity) {
    this.editType = 'update'
    this.model = editEntity
    this.visable = true
  }
  view(editEntity: TEditEntity) {
    this.editType = 'view'
    this.model = editEntity
    this.visable = true
  }
}
export { EditorProps }
