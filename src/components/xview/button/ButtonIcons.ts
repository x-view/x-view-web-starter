import {
  Check,
  CirclePlus,
  Delete,
  Edit,
  Promotion,
  Search,
  Document,
  CaretRight,
  Refresh,
  User,
  Link,
  Finished,
  Close,
  Lock,
  DocumentCopy
} from '@element-plus/icons-vue'

const ButtonIcons = {
  insert: CirclePlus,
  update: Edit,
  view: Document,
  delete: Delete,
  del: Delete,
  remove: Delete,
  search: Search,
  query: Search,
  ok: Check,
  finish: Finished,
  go: Promotion,
  close: Close,
  execute: CaretRight,
  refresh: Refresh,
  user: User,
  link: Link,
  lock: Lock,
  export: DocumentCopy
}

export { ButtonIcons }
