type ButtonBusType = 'insert' | 'edit' | 'view' | 'del' | 'custom'
export default ButtonBusType
