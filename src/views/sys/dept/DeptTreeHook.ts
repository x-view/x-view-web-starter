import { IDeptTreeData, IDeptTreeNodeItem } from '~/types/sys/IDeptTreeNode'
import { reactive, ref } from 'vue'

export default function DeptTreeHook() {
  const tree = reactive<{
    data: IDeptTreeData
    currentNode: IDeptTreeNodeItem
  }>({
    data: [],
    currentNode: null
  })
  const treeRef = ref(null)

  return {
    tree,
    treeRef
  }
}
