/**
 * 全局配置项
 */

export default {
  // 系统名称
  systemName: 'X-View脚手架示例管理系统',

  // 是否通过角色权限控制菜单和按钮
  enablePermission: true,

  //是否通过角色权限控制Api访问(前端过滤)
  enableApiPermission: false,

  //按钮的权限控制方式，支持 disable|hide 默认为disable
  btnPermissionControlType: 'disable',

  // 每页默认数据条数
  pageSize: 20,

  // 左侧菜单宽度，暂不能低于200
  leftMenuWidth: 220,

  //最大打开模块tab的数量
  maxOpenModuleTableCount: 10,

  // 自定义 localStorage 存储变量前缀
  storeKeyPrefix: 'x-view-web-'
}
