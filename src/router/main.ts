import { RouteRecordRaw } from 'vue-router'
export default [
  {
    path: '/login',
    name: 'login',
    component: () => import('~/views/main/Login.vue')
  },
  {
    path: '/',
    component: () => import('~/views/main/Main.vue'),
    name: 'main',
    redirect: '/index',
    children: [
      {
        path: '/index',
        component: () => import('~/views/main/Index.vue'),
        name: 'index',
        meta: {
          caption: '首页'
        }
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    component: () => import('~/views/error/404.vue')
  },
  {
    path: '/500',
    name: '500',
    component: () => import('~/views/error/500.vue')
  }
] as RouteRecordRaw[]
