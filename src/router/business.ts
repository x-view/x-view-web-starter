import { RouteRecordRaw } from 'vue-router'

export default [
  {
    path: '/sys',
    component: () => import('~/views/main/Empty.vue'),
    name: 'sys',
    children: [
      {
        path: '/sys/user',
        name: 'user',
        component: () => import('~/views/sys/user/SysUser.vue')
      },
      {
        path: '/sys/dept',
        name: 'dept',
        component: () => import('~/views/sys/dept/SysDept.vue')
      },
      {
        path: '/sys/resource',
        name: 'resource',
        component: () => import('~/views/sys/resource/SysResource.vue')
      },
      {
        path: '/sys/role',
        name: 'role',
        component: () => import('~/views/sys/role/SysRole.vue')
      },
      {
        path: '/sys/auditlog',
        name: 'auditlog',
        component: () => import('~/views/sys/log/RequestLog.vue')
      }
    ]
  }
] as RouteRecordRaw[]
