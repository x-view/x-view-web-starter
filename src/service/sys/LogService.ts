import { RequestLog } from '~/types/sys/RequestLog'
import { PageQueryResult } from '~/types/PageQueryResult'
import { LogQueryCondition } from '~/types/sys/LogQueryCondition'
import axios from '~/utils/axios'

class UserService {
  private url = {
    query: '/log/query'
  }

  async query(condition: LogQueryCondition): Promise<PageQueryResult<RequestLog>> {
    const { data } = await axios.post<PageQueryResult<RequestLog>>(this.url.query, condition)
    return data
  }
}

export default new UserService()
