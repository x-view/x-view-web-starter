import { SysRole } from '~/types/sys/SysRole'
import { PageQueryResult } from '~/types/PageQueryResult'
import { RoleQueryCondition } from '~/types/sys/RoleQueryCondition'
import axios from '~/utils/axios'

class RoleService {
  private url = {
    query: '/role/query',
    insert: '/role/insert',
    update: '/role/update',
    del: '/role/delete',
    savePermission: '/role/save-permissions',
    permissions: '/role/permissions',
    list: '/role/list'
  }

  async query(condition: RoleQueryCondition): Promise<PageQueryResult<SysRole>> {
    const { data } = await axios.post<PageQueryResult<SysRole>>(this.url.query, condition)
    return data
  }

  async list(): Promise<SysRole[]> {
    const { data } = await axios.post<SysRole[]>(this.url.list)
    return data
  }

  async insert(sysUser: SysRole): Promise<void> {
    await axios.post(this.url.insert, sysUser)
  }

  async update(sysUser: SysRole): Promise<void> {
    await axios.post(this.url.update, sysUser)
  }

  async del(ids: string[]): Promise<void> {
    await axios.post(this.url.del, ids)
  }

  async rolePermissions(roleId: string): Promise<string[]> {
    const { data } = await axios.post<string[]>(this.url.permissions, { id: roleId })
    return data
  }

  async savePermissions(roleId: string, resourceIds: string[]): Promise<void> {
    await axios.post(this.url.savePermission, {
      roleId,
      resourceIds
    })
  }
}

export default new RoleService()
