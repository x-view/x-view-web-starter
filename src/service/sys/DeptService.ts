import { SysDept } from '~/types/sys/SysDept'
import { PageQueryResult } from '~/types/PageQueryResult'
import { IDeptTreeData } from '~/types/sys/IDeptTreeNode'
import { DeptQueryCondition } from '~/types/sys/DeptQueryCondition'
import axios from '~/utils/axios'

class DeptService {
  private url = {
    tree: '/dept/tree',
    query: '/dept/query',
    insert: '/dept/insert',
    update: '/dept/update',
    disable: '/dept/disable',
    enable: '/dept/enable'
  }

  async queryTree(): Promise<IDeptTreeData> {
    const { data } = await axios.post<IDeptTreeData>(this.url.tree)
    return data
  }

  async query(condition: DeptQueryCondition): Promise<PageQueryResult<SysDept>> {
    const { data } = await axios.post<PageQueryResult<SysDept>>(this.url.query, condition)
    return data
  }

  async insert(sysDept: SysDept): Promise<void> {
    await axios.post(this.url.insert, sysDept)
  }

  async update(sysDept: SysDept): Promise<void> {
    await axios.post(this.url.update, sysDept)
  }

  async disable(ids: string[]): Promise<void> {
    await axios.post(this.url.disable, ids)
  }

  async enable(ids: string[]): Promise<void> {
    await axios.post(this.url.enable, ids)
  }

  mapDeptType(deptType: number): string {
    if (deptType === null || deptType === undefined) return ''
    switch (deptType) {
      case 0:
        return '部门'
      case 1:
        return '单位'
      default:
        return deptType.toString()
    }
  }
}

export default new DeptService()
