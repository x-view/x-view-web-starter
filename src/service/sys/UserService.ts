import { ITableColumn } from './../../components/xview/table/ITableColumn'
import { SysUser } from '~/types/sys/SysUser'
import { PageQueryResult } from '~/types/PageQueryResult'
import { UserQueryCondition } from '~/types/sys/UserQueryCondition'
import { ColumnConfig, exportViaExcel } from '~/utils/excel'
import axios from '~/utils/axios'

class UserService {
  private url = {
    query: '/user/query',
    export: '/user/export',
    insert: '/user/insert',
    update: '/user/update',
    disable: '/user/disable',
    enable: '/user/enable',
    roles: '/user/roles',
    saveRoles: '/user/saveRoles',
    resetPassword: '/user/resetPassword'
  }

  async query(condition: UserQueryCondition): Promise<PageQueryResult<SysUser>> {
    const { data } = await axios.post<PageQueryResult<SysUser>>(this.url.query, condition)
    return data
  }

  export(condition: UserQueryCondition, columns: ITableColumn[], columnResolver: (column: ColumnConfig) => void) {
    exportViaExcel(this.url.export, columns, condition, columnResolver)
  }

  async insert(sysUser: SysUser): Promise<void> {
    await axios.post(this.url.insert, sysUser)
  }

  async update(sysUser: SysUser): Promise<void> {
    await axios.post(this.url.update, sysUser)
  }

  async disable(ids: string[]): Promise<void> {
    await axios.post(this.url.disable, ids)
  }

  async enable(ids: string[]): Promise<void> {
    await axios.post(this.url.enable, ids)
  }

  async userRoles(userId: string): Promise<string[]> {
    const { data } = await axios.post<string[]>(this.url.roles, {
      id: userId
    })
    return data
  }

  async saveRoles(userId: string, roleIds: string[]): Promise<void> {
    await axios.post(this.url.saveRoles, {
      userId,
      roleIds
    })
  }
}

export default new UserService()
