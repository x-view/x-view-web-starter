import { SysResource } from '~/types/sys/SysResource'
import { PageQueryResult } from '~/types/PageQueryResult'
import { ResourceQueryCondition } from '~/types/sys/ResourceQueryCondition'
import axios from '~/utils/axios'
import { ITreeNode } from '~/types/ui/tree/ITreeNode'

class ResourceService {
  private url = {
    tree: '/resource/tree',
    query: '/resource/query',
    insert: '/resource/insert',
    update: '/resource/update',
    del: '/resource/delete'
  }

  async queryTree(): Promise<ITreeNode[]> {
    const { data } = await axios.post<ITreeNode[]>(this.url.tree)
    return data
  }

  async query(condition: ResourceQueryCondition): Promise<PageQueryResult<SysResource>> {
    const { data } = await axios.post<PageQueryResult<SysResource>>(this.url.query, condition)
    return data
  }

  async insert(sysResource: SysResource): Promise<void> {
    await axios.post(this.url.insert, sysResource)
  }

  async update(sysResource: SysResource): Promise<void> {
    await axios.post(this.url.update, sysResource)
  }

  async del(ids: string[]): Promise<void> {
    await axios.post(this.url.del, ids)
  }

  resTypes = {
    menu: '菜单',
    directory: '目录',
    function: '功能'
  }

  resTypeText(resType: string): string {
    return this.resTypes[resType]
  }
  resTypeSelectSource() {
    const map = []
    for (const key in this.resTypes) {
      map.push({
        value: key,
        text: this.resTypes[key]
      })
    }
    return map
  }
}

export default new ResourceService()
