import DeptService from './DeptService'
import UserService from './UserService'
import ResourceService from './ResourceService'
import RoleService from './RoleService'
import LogService from './LogService'
export { DeptService, UserService, ResourceService, RoleService, LogService }
