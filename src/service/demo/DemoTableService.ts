import { PageQueryResult } from '~/types/PageQueryResult'
import { DemoTableQueryCondition } from '~/types/demo/DemoTableQueryCondition'
import { DemoTable } from '~/types/demo/DemoTable'

import axios from '~/utils/axios'

class DemoTableService {
  private url = 'demo/table'

  async query(condition: DemoTableQueryCondition): Promise<PageQueryResult<DemoTable>> {
    const { data } = await axios.post<PageQueryResult<DemoTable>>(this.url, condition)
    return data
  }

  async save(demoTable: DemoTable): Promise<void> {
    await axios.put(this.url, demoTable)
  }

  async delById(id: string): Promise<void> {
    await axios.delete(this.url + '/' + id)
  }

  async del(ids: string[]): Promise<void> {
    await axios.post(this.url + '/delete', ids)
  }
}

export default new DemoTableService()
