export interface IResponseData<T> {
  success: boolean
  status: number
  code?: number
  data: T
  message?: string
  path?: string
  timestamp?: string
}
