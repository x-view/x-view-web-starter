export class PageQueryResult<T> {
  /**
   * 当前页码
   */
  currentPage: number
  /**
   * 总页数
   */
  pagesTotal: number
  /**
   * 记录总数量
   */
  recordsTotal: number
  /**
   * 数据
   */
  data: T[]
}
