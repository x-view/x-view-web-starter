export class PageQueryCondition {
  paging?: Pagination
  constructor() {
    this.paging = new Pagination()
  }
}

export class Pagination {
  pageSize: number
  current: number
  orderByString?: string

  constructor() {
    this.pageSize = 15
    this.current = 1
  }
}
