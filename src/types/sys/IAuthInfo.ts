import { IResource } from './IResource'
import { ILoginUser } from './ILoginUser'

export interface IAuthInfo {
  token: string
  user: ILoginUser
  department: any
  organization: any
  roles: string[]
  resources: IResource[]
}
