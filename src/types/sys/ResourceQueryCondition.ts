import { PageQueryCondition } from '../PageQueryCondition'
export class ResourceQueryCondition extends PageQueryCondition {
  name: string
  parentResourceId: string
}
