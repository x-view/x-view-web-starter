export interface RequestLog {
  id: number
  requestTime: string
  requestPath: string
  userId: string
  userName: string
  requestIp: string
  params: string
}
