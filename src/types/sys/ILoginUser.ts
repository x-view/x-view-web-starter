export interface ILoginUser {
  id: string
  userName: string
  realName: string
  deptId: string
  mobileNumber: string
  expDate: string
  initialized: number
  idCode: string
  status: number
  sort: number
  userType: number
  updateTime: string
}
