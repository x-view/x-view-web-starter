import { ITreeNode } from '~/types/ui/tree/ITreeNode'
type IDeptTreeData = IDeptTreeNodeItem[]
interface IDeptTreeNodeItem extends Omit<ITreeNode, 'children'> {
  children: IDeptTreeNodeItem[] | boolean
  deptType: number
  levelCode: string
}

export { IDeptTreeData, IDeptTreeNodeItem }
