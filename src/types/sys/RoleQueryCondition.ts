import { PageQueryCondition } from '../PageQueryCondition'
export class RoleQueryCondition extends PageQueryCondition {
  name: string
}
