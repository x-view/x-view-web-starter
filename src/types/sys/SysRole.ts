export interface SysRole {
  id: string
  roleKey: string
  roleName: string
  remark?: string
  sort?: number
}
