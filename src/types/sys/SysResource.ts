export interface SysResource {
  id: string
  parentResId: string
  resName: string
  resKey: string
  resType: string
  sort: number
  icon: string
  resPath?: string
}
