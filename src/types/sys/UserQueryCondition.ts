import { PageQueryCondition } from '../PageQueryCondition'
export class UserQueryCondition extends PageQueryCondition {
  name: string
  idCode: string
  levelCode: string
  status?: number = 0
}
