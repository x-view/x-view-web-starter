import { PageQueryCondition } from '../PageQueryCondition'
export class DeptQueryCondition extends PageQueryCondition {
  name: string
  parentId: string
  status?: number = 0
}
