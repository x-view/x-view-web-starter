import { PageQueryCondition } from '../PageQueryCondition'
export class LogQueryCondition extends PageQueryCondition {
  timeRange: [string, string]
  userId: string
  path: string
  params: string
}
