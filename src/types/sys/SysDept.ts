export interface SysDept {
  id: string
  name: string
  shortName: string
  levelCode: string
  sort: number
  status: number
  deptType: number
  orgType?: string
  updateTime: string
  parentId?: string
  remark?: string
}
