export interface SysUser {
  id: string
  userName: string
  realName: string
  deptId: string
  mobileNumber: string
  expDate: string
  initialized: number
  idCode: string
  status: number
  sort: number
  userType: number
  updateTime: string
  deptName: string
  lastLoginTime?: string
  duty?: string
  sex?: number
  passwdInit?: string
}
