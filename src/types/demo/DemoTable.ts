export interface DemoTable {
  id?: string
  name: string
  numberField1?: number
  numberField2?: number
  remark?: string
  textField1?: string
  textField2?: string
}
