import { PageQueryCondition } from '../PageQueryCondition'
export class DemoTableQueryCondition extends PageQueryCondition {
  name: string
  parentId: string
  status?: number = 0
}
