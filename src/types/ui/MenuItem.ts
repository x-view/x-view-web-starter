export interface MenuItem {
  name: string
  caption: string
  path: string
  icon: string
  children: MenuItem[]
}
