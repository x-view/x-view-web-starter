interface ITreeNode {
  id: string
  text: string
  state: TreeNodeState
  children: ITreeNode[]
}

class TreeNodeState {
  open: boolean
}
export { ITreeNode, TreeNodeState }
