type EditType = 'insert' | 'update' | 'view' | 'del'|'query'
const EditTypeText = {
  insert: '新增',
  update: '编辑',
  view: '查看',
  del: '删除',
  query:'查询'
}
export { EditType, EditTypeText }
