import * as EditorHooks from './EditorHooks'
import * as TableHooks from './TableHooks'
export { EditorHooks, TableHooks }
