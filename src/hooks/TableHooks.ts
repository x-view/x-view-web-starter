import { reactive } from 'vue'
import { ITableColumn } from '~/components/xview/table/ITableColumn'
import { TableProps } from '~/components/xview/table/TableProps'

function initTable<TRowData>(columns: ITableColumn[]) {
  const props = new TableProps<TRowData>()
  props.tableColumns = props.tableColumns.concat(columns)
  const table = reactive<TableProps<TRowData>>(props)
  return table
}

export { initTable }
