import { reactive, ref } from 'vue'
import { EditorProps } from '~/components/xview/editor/EditorProps'

function init<TEditEntity>() {
  const props = new EditorProps<TEditEntity>()
  const editor = reactive<EditorProps<TEditEntity>>(props)
  return editor
}

export { init }
