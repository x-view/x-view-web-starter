import axios from './axios'
import { ITableColumn } from '~/components/xview/table/ITableColumn'

export class ColumnConfig {
  prop: string
  label: string
  width: number
  format: string
  listSource: object
}

/**
 * export page data to excel via post method
 * @param url export data url(via post)
 * @param columns columns config from vue table-config
 * @param condition page querycondition
 * @param columnResolver column resolver callback function , can config listsource or format text
 */
export function exportViaExcel<TCondition>(
  url: string,
  columns: ITableColumn[],
  condition: TCondition,
  columnResolver: (column: ColumnConfig) => void
): void {
  const exportCondition = {
    condition,
    columns: []
  }

  for (let i = 0; i < columns.length; i++) {
    const c = {
      prop: columns[i].prop,
      label: columns[i].label,
      width: columns[i].width
    } as ColumnConfig
    if (columnResolver != undefined) columnResolver(c)
    exportCondition.columns.push(c)
  }
  axios.downloadFileViaPost(url, exportCondition)
}
