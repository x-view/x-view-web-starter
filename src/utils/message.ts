import { ElMessageBox, ElNotification } from 'element-plus'
const COMMON_TITLE = '操作提示'

const notification = (type: 'info' | 'success' | 'warning' | 'error', message: string, title?: string) => {
  title = title || COMMON_TITLE
  ElNotification({
    type: type,
    message: message,
    title: title
  })
}

const msgUtil = {
  info(message: string, title?: string) {
    notification('info', message, title)
  },
  success(message: string, title?: string) {
    notification('success', message, title)
  },
  warning(message: string, title?: string) {
    notification('warning', message, title)
  },
  error(message: string, title?: string) {
    notification('error', message, title)
  },
  confirm(message: string, title?: string) {
    title = title || COMMON_TITLE
    return ElMessageBox.confirm(message, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    })
  }
}

export default msgUtil
