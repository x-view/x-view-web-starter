import type { ElForm, ElTree, ElTable, ElDialog } from 'element-plus'

import { ref, Ref } from 'vue'

export const tree = (): Ref<InstanceType<typeof ElTree>> => ref<InstanceType<typeof ElTree>>()

export const table = (): Ref<InstanceType<typeof ElTable>> => ref<InstanceType<typeof ElTable>>()

export const form = (): Ref<InstanceType<typeof ElForm>> => ref<InstanceType<typeof ElForm>>()

export const dialog = (): Ref<InstanceType<typeof ElDialog>> => ref<InstanceType<typeof ElDialog>>()

const elpRefsUtil = { tree, form, table, dialog }

export default elpRefsUtil
