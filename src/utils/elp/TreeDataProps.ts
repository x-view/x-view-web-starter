import { TreeNodeData } from 'element-plus/es/components/tree/src/tree.type'

class TreeDataProps {
  value: string | Function
  label: string | ((data: TreeNodeData, node: Node) => string)
  children: string
  disabled?: string | Function
  isLeaf?: string | Function
  class?: string | Function
}
const defaultTreeDataProps: TreeDataProps = {
  value: 'id',
  label: 'text',
  children: 'children'
}

export { defaultTreeDataProps, TreeDataProps }
