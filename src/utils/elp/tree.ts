import { reactive, ref, Ref } from 'vue'
import { ITreeNode } from '~/types/ui/tree/ITreeNode'

// import { TreeDataProps, defaultTreeDataProps } from './TreeDataProps'
import type { ElTree } from 'element-plus'
import { TreeOptionProps } from 'element-plus/es/components/tree/src/tree.type'

const defaultProps = {
  value: 'id',
  label: 'text',
  children: 'children'
}

class TreeProp {
  nodeKey = 'id'
  data: ITreeNode[] = []
  defaultCheckedKeys: string[] = []
  defaultExpandedKeys: string[] = []
  currentNodeKey?: string
  checkStrictly = false
  props: TreeOptionProps = defaultProps
  showCheckbox = false
  checkChange: Function
}

class TreeUtil {
  static defaultProps(): TreeProp {
    return reactive(new TreeProp())
  }

  static createRef(): Ref<InstanceType<typeof ElTree>> {
    return ref<InstanceType<typeof ElTree>>(null)
  }
}

export { TreeProp, TreeUtil }
