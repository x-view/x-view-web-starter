import { userStore } from '~/store'
export function setupAuthDirective(app) {
  let auths: string[] = undefined

  // 单个权限验证（v-auth="xxx"）
  app.directive('auth', {
    async mounted(el, binding) {
      if (auths === undefined) {
        auths = await userStore().getAuths()
      }
      if (!auths.some((v) => v === binding.value)) el.parentNode.removeChild(el)
    }
  })

  app.directive('auths', {
    async mounted(el, binding) {
      if (auths === undefined) {
        auths = await userStore().getAuths()
      }
      let flag = false
      auths.map((val) => {
        binding.value.map((v) => {
          if (val === v) flag = true
        })
      })
      if (!flag) el.parentNode.removeChild(el)
    }
  })
}
