### 项目发布

#### Setp1： 编译源代码，执行命令

```
# 这里示例使用npm，自行打包的时候使用本地熟悉工具如cnpm yarn等
npm run build
```

#### Setp2: 打包发布目录并拷贝到服务器 nginx 目录

```
# 这个步骤为打包为压缩包，可以省略，最终目的为将./dist/目录拷贝到目标服务器
tar -zcvf x-view.tar.gz ./dist/
# 示例通过scp进行拷贝，实际情况可自行拷贝
scp x-view.tar.gz root@192.168.0.x:/www/
# 解压
tar -zxvf x-view.tar.gz
```

#### Setp3: 配置 nginx 配置文件并启动

```
nginx -s reload
```

以下为 nginx 配置文件示例

```
  server {
        listen       3000;
        server_name  localhost;

        location / {
            root   /www/;
            index  index.html index.htm;
        }

        location /api/ {
            proxy_pass http://localhost:18090/;
            proxy_redirect off;
            proxy_set_header Host $host:$server_port;
            proxy_set_header X-Real_IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

    }
```
